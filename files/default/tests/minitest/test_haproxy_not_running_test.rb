describe "haproxy_lwrp" do
  include MiniTest::Chef::Assertions
  include MiniTest::Chef::Context
  include MiniTest::Chef::Resources
  describe "haproxy_lwrp::test_haproxy_not_running" do
    it "should have haproxy installed" do
      package("haproxy").must_be_installed
    end
  end
end
