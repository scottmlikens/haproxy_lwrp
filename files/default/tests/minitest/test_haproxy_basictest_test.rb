describe "haproxy_lwrp" do
  include MiniTest::Chef::Assertions
  include MiniTest::Chef::Context
  include MiniTest::Chef::Resources
  describe "haproxy_lwrp::haproxy_basictest" do
    it "should have haproxy installed" do
      package("haproxy").must_be_installed
    end
    it "should have a service called carbon up" do
      service("adserver").must_be_running
    end
  end
end
